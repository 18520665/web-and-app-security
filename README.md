# Web-and-App Security

Bảo mật web và ứng dụng - UIT InSecLab - Tài liệu tổng hợp đồ án môn học

Các nhóm sinh viên đã báo cáo đồ án môn học thực hiện tải các tài liệu liên quan lên nhánh (branch) **sinhvien**.
Yêu cầu:

+ Tạo thư mục có tiêu đề cho nhóm. ví dụ: STTBaocao_Nhom01_SQLInjection, tương ứng trong mã lớp của nhóm (chú ý đúng thư mục mã lớp mà mình đang học)
+ Tải slide/báo cáo + source code chương trình demo vào thư mục vừa tạo trên Gitlab